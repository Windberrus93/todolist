// React
import React from 'react';
import AppIcon from '../../assets/todo icon.svg';
// CSS
import './Header.css';

const navigateTo = (history, path) => {
    history.push(path);
}

const header = (props) => {
  const exitButton = (props.hasButton) ? (
      <button
        type="button"
        onClick={() => navigateTo(props.history, '/login')}
        className="signOutButton">Sign Out
      </button>
    ) : '';
  return (
    <div>
      <header>
        <nav className="navBar">
          <div className="titleAndIcon">
            <img alt="app icon" src={AppIcon}/>
            <h2>{props.pageTitle}</h2>
          </div>
          {exitButton}
        </nav>
      </header>
      <hr/>
    </div>
  );
}

export default header;