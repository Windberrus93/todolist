// React
import React from 'react';
import { Route, Switch } from 'react-router-dom';
// Pages
import Login from './pages/Login/Login';
import Register from './pages/Register/Register';
import TodoList from './pages/TodoList/TodoList';
// Styles
import './App.css';

const App = () => {
    return (
      <div className="App">
        <Switch>
          <Route exact path="/" component={Login}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/todo" component={TodoList}/>
        </Switch>
      </div>
    );
}

export default App;
