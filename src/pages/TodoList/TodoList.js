// React
import React from 'react';
// Components
import Header from '../../components/Header/Header';
// CSS
import './TodoList.css';
import { FaCheck, FaTimes } from "react-icons/fa";

class todoList extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        input: '',
        target: '',
        todoList: []
      }
    }
    
    handleInputChange(event) {
      const value = event.target.value;
      const newState = (this.state.target === '') ? {
        input: value, 
        target: event.target
      } : {
        input: value
      };
      this.setState(newState);
    }

    add() {
      const actualState = {...this.state};
      if (this.state.input !== '') {
        const {input, target, todoList} = actualState;
        target.value = '';
        todoList.push({ task: input, isDone: false });
        this.setState({ input: '', todoList: todoList });
      }
    }

    taskDone(id) {
      const actualState = {...this.state};
      const {todoList} = actualState;
      todoList[id].isDone = !todoList[id].isDone;
      this.setState({ todoList: todoList });
    }

    removeTask(id) {
      const actualState = {...this.state};
      const {todoList} = actualState;
      const newTodoList = todoList.filter((todoItem, index) => {
        console.log(todoItem);
        return index !== id;
      });
      this.setState({ todoList: newTodoList })
    }

    render() {
      const items = this.state.todoList.map((todoItem, index) => {
        if (todoItem.isDone) {
          return (
            <div key={index} className="listItems">
              <FaCheck className="faIcons checked" onClick={() => this.taskDone(index)}/>
              <p className="checked"><del>{todoItem.task}</del></p>
              <FaTimes className="faIcons times" onClick={() => this.removeTask(index)}/>
            </div>
          );
        } else {
          return (
            <div key={index} className="listItems">
              <FaCheck className="faIcons check" onClick={() => this.taskDone(index)}/>
              <p className="check">{todoItem.task}</p>
              <FaTimes className="faIcons times" onClick={() => this.removeTask(index)}/>
            </div>
          );
        }
      });
      return (
        <div>
          <Header
            pageTitle="Todo List"
            hasButton={true}
            history={this.props.history}
            className="header">
          </Header>
          <br/>
          <div className="wrapTodoList">
            <div className="roundTodoList">
              {items}
            </div>
          </div>
          <br/>
          <div className="wrapInput">
            <input
              type="text"
              placeholder="add a new todo..."
              onChange={(event) => this.handleInputChange(event)}
              className="sapcePlaceholder">
            </input>
          </div>
          <div className="wrapButton">
            <button type="button" onClick={() => this.add()} className="addButton">Add</button>
          </div>
        </div>
      );
    }
}

export default todoList;