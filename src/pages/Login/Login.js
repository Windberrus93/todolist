// React
import React from 'react';
// Components
import Header from '../../components/Header/Header';
// CSS
import './Login.css';

class login extends React.Component {

  constructor(props) {
    super(props);
    // this.navigateTo = this.navigateTo.bind(this);
    this.state = {
      user: '',
      password: '',
      isSignedIn: false
    };
  }

  handleSubmit(event) {
    event.preventDefault();
    /* // I tried destructuration here, didn't work
    const { user, password } = this.state; */
    if ((this.state.user === 'hola@gmail.com') && (this.state.password === 'hola1234')) {
      this.setState({
        isSignedIn: true
      });
      this.navigateTo('/todo');
    }
  }

  handleInputChange(event, field) {
    const value = event.target.value;
    const name = field;
    this.setState({
      [name]: value
    });
  }

  navigateTo(path) {
    this.props.history.push(path);
  }

  render() {
    return (
        <div>
          <Header pageTitle="Sign In" className="header"></Header>
          <br/>
          <form onSubmit={(event) => this.handleSubmit(event)} className="SignInForm">
            <input
              type="text"
              placeholder="User"
              onChange={(event) => this.handleInputChange(event, 'user')}
              className="sapceIt">
            </input>
            <input
              type="password"
              placeholder="Password"
              onChange={(event) => this.handleInputChange(event, 'password')}
              className="sapceIt">
            </input>
            <div className="buttons">
              <button
                type="submit"
                className="lefthButton">Sign In
              </button>
              <button
                type="button"
                className="rightButton"
                onClick={() => this.navigateTo('/register')}>Register
              </button>
            </div>
          </form>
        </div>
    );
  }
}

export default login;