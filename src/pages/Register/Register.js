// React
import React from 'react';
// Components
import Header from '../../components/Header/Header';
// CSS
import './Register.css';

class register extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: '',
      password: '',
      confirmPassword: '',
      isSignedIn: false
    };
  }

  handleSubmit(event) {
    event.preventDefault();
    if ((this.state.user !== '') && (this.state.password !== '') && (this.state.confirmPassword !== ''))
      if (this.state.password === this.state.confirmPassword) {
        this.navigateTo('/todo');
      }
  }

  handleInputChange(event, field) {
    const value = event.target.value;
    const name = field;
    this.setState({
      [name]: value
    });
  }

  navigateTo(path) {
    this.props.history.push(path);
  }

  render() {
    return (
      <div>
        <Header pageTitle="Register" className="header"></Header>
        <br/>
        <form onSubmit={(event) => this.handleSubmit(event)} className="registerForm">
          <input
            type="text"
            placeholder="User"
            onChange={(event) => this.handleInputChange(event, 'user')}
            className="sapceIt">
          </input>
          <input
            type="password"
            placeholder="Password"
            onChange={(event) => this.handleInputChange(event, 'password')}
            className="sapceIt">
          </input>
          <input
            type="password"
            placeholder="Confirm password"
            onChange={(event) => this.handleInputChange(event, 'confirmPassword')}
            className="sapceIt">
          </input>
          <div className="buttons">
            <button
              type="submit"
              className="lefthButton">Register
            </button>
            <button
              type="button"
              className="rightButton"
              onClick={() => this.navigateTo('/login')}>To sign In
            </button>
          </div>
        </form>
      </div>
    )
  }
}

export default register;